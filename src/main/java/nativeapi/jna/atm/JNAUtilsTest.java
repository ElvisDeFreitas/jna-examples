package nativeapi.jna.atm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

/**
 * @author elvis
 * @version $Revision: $<br/>
 *          $Id: $
 * @since 10/17/16 3:36 PM
 */
public class JNAUtilsTest {

//    http://man7.org/linux/man-pages/man2/readlink.2.html
//    https://linux.die.net/man/2/stat
//    http://unix.superglobalmegacorp.com/Net2/newsrc/sys/stat.h.html

    static final int S_IFMT =  0170000;
    static final int S_IFLNK = 0120000;


    public interface CLibrary extends Library {
        CLibrary INSTANCE = getInstance();
        int gethostname(byte[] name, int nameLength);
        Pointer fopen(String name, String mode);
        Mtent.ByReference getmntent(Pointer FILE);
//        int stat(String path, Stat.ByReference stats);
        int syscall(int number, Object ...args);
    }

//    public static void main3(String[] args) throws NoSuchAlgorithmException, IOException {
//        final boolean b = compareFilesCheckSum(new File("/tmp/tomcat.5674744809731701627.9292"),
//                new File("/tmp/tomcat.5674744809731701627.9292"));
//
//        System.out.println("equals files: " + b);
//    }

//    /**
//     * Verifica se dois arquivos/diretorios sao o mesmo
//     * @param a
//     * @param b
//     * @return
//     * @throws IOException
//     * @throws NoSuchAlgorithmException
//     */
//    public static boolean compareFilesCheckSum(File a, File b) throws IOException, NoSuchAlgorithmException {
//        return Arrays.equals(checkSum(a), checkSum(b));
//    }
//
//    private static byte[] checkSum(final File file) throws IOException, NoSuchAlgorithmException {
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        try (
//            final DigestInputStream dis = new DigestInputStream(new FileInputStream(file), md)
//        ){
//            return md.digest();
//        }
//    }

    public static void main(String[] args) throws IllegalAccessException {


//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());
//        CLibrary.INSTANCE.syscall(83, "/tmp/elvis-" + System.nanoTime());

        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
        System.out.println("/tmp/: " + isSymlink("/tmp/"));
//        System.out.println("/tmp/tomcat-docbase.991133291043579086.3802: " + isSymlink("/tmp/tomcat-docbase.991133291043579086.3802"));
//        System.out.println("/tmp/tomcat-docbase.991133291043579086.3802.link: " + isSymlink("/tmp/tomcat-docbase.991133291043579086.3802.link"));


//        CLibrary.INSTANCE.syscall(6, "/home/elvis/workspace/hosts/", bytes);

//        System.out.println(Arrays.toString(bytes));
//        System.out.println(stat);
//
//        for(final Field f: Stat.class.getDeclaredFields()){
//            if(!f.isSynthetic())
//                System.out.println(f.getName() + "=" + f.get(stat));
//        }

//        System.out.println(stat.st_dev);
//        System.out.println(stat.st_ino);
//        System.out.println(stat.st_mode);
//        System.out.println(stat.st_nlink);
//        System.out.println(Arrays.toString(stat.st_dev));
//        System.out.println("exitCode: " + returnCode);
    }

    public static boolean isSymlink(final String path) {
        final Stat.ByReference stat = new Stat.ByReference();
        final int returnCode = CLibrary.INSTANCE.syscall(4, path, stat);
        if(returnCode != 0){
            throw new RuntimeException("Falha ao verificar o arquivo: " + path);
        }
//        return (stat.st_mode & S_IFMT) == S_IFLNK;
        return false;
    }

    static CLibrary INSTANCE = (CLibrary) Native.loadLibrary("c", CLibrary.class);
//    (CLibrary) Native.loadLibrary("c", CLibrary.class)
    private static synchronized CLibrary getInstance() {
        System.out.println("get instance");
        if(INSTANCE == null){
            INSTANCE = (CLibrary) Native.loadLibrary("c", CLibrary.class);
        }
        return INSTANCE;
    }

    public static void main2(String[] args) {

        final byte[] hostname = new byte[30];
        int returnCode = CLibrary.INSTANCE.gethostname(hostname, hostname.length);
        System.out.printf("%s - %d\n", new String(hostname), returnCode);

        final Pointer mountFile = CLibrary.INSTANCE.fopen("/home/elvis/dev/projects/java-native-examples/src/main/resources/mnt/proc_mounts", "r");
//        final Pointer mountFile = CLibrary.INSTANCE.fopen("/proc/mounts", "r");
        Mtent.ByReference mtent;
        while( (mtent = CLibrary.INSTANCE.getmntent(mountFile)) != null ){
            System.out.println(mtent);
        }

    }

    public static class Mtent extends Structure {

        public String fileSystemName;
        public String mnt_dir;
        public String mnt_type;
        public String mnt_opts;
        public int mnt_freq;
        public int mnt_passno;

        @Override
        protected List getFieldOrder() {
            List<String> fieds = new ArrayList<>();
            for(final Field f: Mtent.class.getDeclaredFields()){
                if(!f.isSynthetic())
                    fieds.add(f.getName());
            }
            return fieds;
        }

        public static class ByReference extends Mtent implements Structure.ByReference {}
        public static class ByValue extends Mtent implements Structure.ByValue {}
    }


    public static class Stat extends Structure {

//        public byte[] bytes = new byte[150];

//        public byte[] st_dev = new byte[8]; /* ID of device containing file */
//        public byte[] st_ino = new byte[8]; /* inode number */
//        public byte[] st_mode = new byte[8]; /* protection */
//        public byte[] st_nlink = new byte[4]; /* number of hard links */
//        public byte[] st_uid = new byte[8]; /* user ID of owner */
//        public byte[] st_gid = new byte[4]; /* group ID of owner */
//        public byte[] st_rdev = new byte[8]; /* device ID (if special file) */
//        public byte[] st_size = new byte[8]; /* total size, in bytes */
//        public byte[] st_blksize = new byte[8]; /* blocksize for file system I/O */
//        public byte[] st_blocks = new byte[8]; /* number of 512B blocks allocated */
//        public byte[] st_atime = new byte[8]; /* time of last access */
//        public byte[] st_mtime = new byte[8]; /* time of last modification */
//        public byte[] st_ctime = new byte[8]; /* time of last status change */





//            public long st_dev ; /* ID of device containing file */
//            public long st_ino ; /* inode number */
//            public long st_mode ; /* protection */
//            public long  st_nlink ; /* number of hard links */
//            public long st_uid ; /* user ID of owner */
//            public long st_gid ; /* group ID of owner */
//            public long st_rdev ; /* device ID (if special file) */
//            public long st_size ; /* total size, in bytes */
//            public long st_blksize ; /* blocksize for file system I/O */
//            public long st_blocks ; /* number of 512B blocks allocated */
//            public long st_atime ; /* time of last access */
//            public long st_mtime ; /* time of last modification */
//            public long st_ctime ; /* time of last status change */




//        public int st_dev; /* ID of device containing file */
//        public byte[] st_ino = new byte[8]; /* inode number */
//        public byte[] st_nlink = new byte[8]; /* number of hard links  */
//        public byte[] st_mode = new byte[4]; /* protection */



//        public long st_dev; /* ID of device containing file */
//        public long st_ino; /* inode number */
//        public long st_nlink; /* number of hard links  */
//        public int st_mode; /* protection */





//        public int st_uid; /* user ID of owner */
//        public int st_gid; /* group ID of owner */
//        public long st_rdev; /* device ID (if special file) */
//        public long st_size; /* total size, in bytes */
//        public long st_blksize; /* blocksize for file system I/O */
//        public long st_blocks; /* number of 512B blocks allocated */
//        public long st_atime; /* time of last access */
//        public long st_mtime; /* time of last modification */
//        public int st_ctime; /* time of last status change */

        public static class ByReference extends Stat implements Structure.ByReference {}

        @Override
        protected List getFieldOrder() {
            List<String> fieds = new ArrayList<>();
            for(final Field f: Stat.class.getDeclaredFields()){
                if(!f.isSynthetic())
                    fieds.add(f.getName());
            }
            return fieds;
        }

//        @Override
//        protected List getFieldOrder() {
//            return Arrays.asList("st_dev", "st_ino", "st_nlink", "st_mode", "st_uid", "st_gid", "st_rdev", "st_size", "st_blksize", "st_blocks", "st_atime", "st_mtime", "st_ctime");
//        }
    }


}
